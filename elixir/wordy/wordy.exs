defmodule Wordy do
  @type operation :: String.t()
  @type operand :: integer
  @type stack_item :: operation | operand
  @type stack :: list(stack_item)

  @fun %{
    "+" => &+/2,
    "-" => &-/2,
    "*" => &*/2,
    "/" => &//2
  }

  @doc """
  Calculate the math problem in the sentence.
  """
  @spec answer(String.t()) :: integer
  def answer(question) do
    question |> parse |> eval
  end

  @spec parse(String.t()) :: stack
  defp parse(question) do
    question |> reduce |> to_parts
  end

  @spec reduce(String.t()) :: String.t()
  defp reduce(question) do
    question
    |> String.replace("What is ", "")
    |> String.replace("plus", "+")
    |> String.replace("minus", "-")
    |> String.replace("divided by", "/")
    |> String.replace("multiplied by", "*")
    |> String.replace("?", "")
  end

  @spec to_parts(String.t()) :: stack
  defp to_parts(reduced) do
    reduced
    |> String.split()
    |> Enum.map(&convert/1)
  end

  @spec convert(String.t()) :: stack_item
  defp convert(s) when s in ~w(* / + -), do: s
  defp convert(s), do: String.to_integer(s)

  @spec eval(stack) :: integer
  defp eval([res]), do: res
  defp eval([a, op, b | rest]), do: eval([@fun[op].(a, b) | rest])
end
