defmodule Matrix do
  defstruct matrix: nil

  @doc """
  Convert an `input` string, with rows separated by newlines and values
  separated by single spaces, into a `Matrix` struct.
  """
  @spec from_string(input :: String.t()) :: %Matrix{}
  def from_string(input) do
    data =
      input
      |> String.split("\n")
      |> Enum.map(&String.split/1)
      |> Enum.map(&to_integers/1)

    %Matrix{matrix: data}
  end

  @spec to_integers(list(String.t())) :: list(integer)
  defp to_integers(xs), do: xs |> Enum.map(&String.to_integer/1)

  @doc """
  Write the `matrix` out as a string, with rows separated by newlines and
  values separated by single spaces.
  """
  @spec to_string(matrix :: %Matrix{}) :: String.t()
  def to_string(%Matrix{matrix: matrix}) do
    matrix
    |> Enum.map(&Enum.join(&1, " "))
    |> Enum.join("\n")
  end

  @doc """
  Given a `matrix`, return its rows as a list of lists of integers.
  """
  @spec rows(matrix :: %Matrix{}) :: list(list(integer))
  def rows(%Matrix{matrix: matrix}), do: matrix

  @doc """
  Given a `matrix` and `index`, return the row at `index`.
  """
  @spec row(matrix :: %Matrix{}, index :: integer) :: list(integer)
  def row(%Matrix{matrix: matrix}, index), do: Enum.at(matrix, index)

  @doc """
  Given a `matrix`, return its columns as a list of lists of integers.
  """
  @spec columns(matrix :: %Matrix{}) :: list(list(integer))
  def columns(%Matrix{matrix: matrix}) do
    transpose(matrix)
  end

  @spec transpose(list(list())) :: list(list())
  defp transpose(xss) do
    xss
    |> Enum.flat_map(&Enum.with_index/1)
    |> Enum.group_by(&index/1, &value/1)
    |> Enum.to_list()
    |> Enum.map(fn {_idx, col_as_row} -> col_as_row end)
  end

  defp index({_val, idx}), do: idx
  defp value({val, _idx}), do: val

  @doc """
  Given a `matrix` and `index`, return the column at `index`.
  """
  @spec column(matrix :: %Matrix{}, index :: integer) :: list(integer)
  def column(%Matrix{matrix: matrix}, index), do: matrix |> transpose |> Enum.at(index)
end
