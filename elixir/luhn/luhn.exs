defmodule Luhn do
  @doc """
  Checks if the given number is valid via the luhn formula
  """
  @spec valid?(String.t()) :: boolean
  def valid?(number) do
    with false <- String.match?(number, ~r/[^\d ]/),
         number <- String.replace(number, " ", ""),
         parts <- String.graphemes(number),
         digits <- Enum.map(parts, &String.to_integer/1),
         true <- length(digits) > 1 do
      digits |> checksum |> rem(10) == 0
    else
      _ -> false
    end
  end

  @spec checksum([non_neg_integer]) :: integer
  def checksum(digits) do
    digits
    |> Enum.reverse()
    |> Enum.with_index()
    |> Enum.map(&double_every_second_digit/1)
    |> Enum.sum()
  end

  @spec double_every_second_digit({non_neg_integer, non_neg_integer}) :: non_neg_integer
  def double_every_second_digit({digit, index})
      when rem(index, 2) == 0,
      do: digit

  def double_every_second_digit({digit, _index})
      when 2 * digit > 9,
      do: 2 * digit - 9

  def double_every_second_digit({digit, _index}), do: 2 * digit
end
