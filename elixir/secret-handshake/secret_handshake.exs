defmodule SecretHandshake do
  use Bitwise

  @doc """
  Determine the actions of a secret handshake based on the binary
  representation of the given `code`.

  If the following bits are set, include the corresponding action in your list
  of commands, in order from lowest to highest.

  1 = wink
  10 = double blink
  100 = close your eyes
  1000 = jump

  10000 = Reverse the order of the operations in the secret handshake
  """
  @spec commands(code :: integer) :: list(String.t())
  def commands(code) do
    actions = %{
      0b1 => "wink",
      0b10 => "double blink",
      0b100 => "close your eyes",
      0b1000 => "jump"
    }
    Enum.map(actions,
      fn({bit, action}) ->
        if (bit &&& code) === bit do action end
      end)
    |> Enum.filter(&(&1 != nil))
    |> reverse?((code &&& 0b10000) === 0b10000)
  end

  def reverse?(arr, do_reverse) do
    if do_reverse do
      Enum.reverse(arr)
    else
      arr
    end
  end
end

