defmodule Bob do
  @response %{
    :question => "Sure.",
    :shouting => "Whoa, chill out!",
    :silence => "Fine. Be that way!",
    :something_else => "Whatever."
  }

  def hey(input) do
    clist = String.to_charlist(input)
    cond do
      String.trim(input) == "" -> @response[:silence]
      String.last(input) == "?" -> @response[:question]
      String.upcase(input) == input and !String.match?(input, ~r/^[\s\d,.]*$/) -> @response[:shouting]
      true -> @response[:something_else]
    end
  end
end
