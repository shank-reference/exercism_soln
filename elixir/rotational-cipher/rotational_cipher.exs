defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text
    |> to_charlist
    |> Enum.map(&rotate_char(&1, shift))
    |> to_string
  end

  defp rotate_char(ch, shift) do
    cond do
      ch in ?A..?Z -> 65 + rem(ch-65+shift, 26)
      ch in ?a..?z -> 97 + rem(ch-97+shift, 26)
      true -> ch
    end
  end
end

