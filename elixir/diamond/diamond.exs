defmodule Diamond do
  @doc """
  Given a letter, it prints a diamond starting with 'A',
  with the supplied letter at the widest point.
  """
  @spec build_shape(char) :: String.t()
  def build_shape(letter) do
    offset = letter - ?A

    upper =
      0..offset
      |> Enum.map(&create_line(&1, offset))
      |> Enum.map(&to_string/1)

    bottom = upper |> Enum.reverse() |> Enum.drop(1)

    res = Enum.join(upper ++ bottom, "\n")

    "#{res}\n"
  end

  @spec create_line(integer, integer) :: [char]
  defp create_line(0, offset) do
    List.duplicate(' ', offset) ++ [?A] ++ List.duplicate(' ', offset)
  end

  defp create_line(i, offset) do
    len = 2 * offset + 1

    prefix = List.duplicate(' ', offset - i)
    suffix = List.duplicate(' ', offset - i)
    mid = List.duplicate(' ', len - 2 * (offset - i) - 2)
    ch = [?A + i]

    prefix ++ ch ++ mid ++ ch ++ suffix
  end
end
