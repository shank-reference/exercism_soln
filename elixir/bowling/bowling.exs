defmodule Bowling do
  # """
  # Make it work
  # Make it beautiful
  # Make it fast
  # """

  @type game :: %{
          frames: %{integer => frame},
          pins: integer,
          curr: integer,
          status: status
        }

  @type status :: :not_started | :in_progress | :finished

  @type frame ::
          nil
          | {integer}
          | {integer, integer}
          | {integer, integer, integer}

  @doc """
    Creates a new game of bowling that can be used to store the results of
    the game
  """
  @spec start() :: game
  def start do
    %{
      pins: 10,
      frames: 1..10 |> Enum.zip(List.duplicate(nil, 10)) |> Map.new(),
      curr: 1,
      status: :not_started
    }
  end

  @doc """
    Records the number of pins knocked down on a single roll. Returns `any`
    unless there is something wrong with the given number of pins, in which
    case it returns a helpful message.
  """
  @spec roll(game, integer) :: game | String.t()
  def roll(%{status: :finished} = _game, _roll),
    do: {:error, "Cannot roll after game is over"}

  def roll(_game, roll) when roll < 0, do: {:error, "Negative roll is invalid"}

  def roll(%{pins: pins} = _game, roll) when roll > pins,
    do: {:error, "Pin count exceeds pins on the lane"}

  def roll(game, roll) do
    %{frames: frames, curr: curr} = game

    updated_frames =
      cond do
        frames[curr] == nil ->
          Map.put(frames, curr, {roll})

        tuple_size(frames[curr]) == 1 ->
          {prev} = frames[curr]
          Map.put(frames, curr, {prev, roll})

        curr == 10 and tuple_size(frames[curr]) == 2 and fill_ball?(frames[curr]) ->
          {p1, p2} = frames[curr]
          Map.put(frames, curr, {p1, p2, roll})
      end

    updated_status =
      case updated_frames[curr] do
        {_, _, _} when curr == 10 -> :finished
        {x, y} when curr == 10 and x + y < 10 -> :finished
        _ -> :in_progress
      end

    updated_pins =
      cond do
        frames[curr] == nil ->
          if roll == 10, do: 10, else: 10 - roll

        tuple_size(frames[curr]) == 1 ->
          if curr == 10 and frames[curr] == {10} do
            if roll == 10, do: 10, else: 10 - roll
          else
            10
          end

        curr == 10 and tuple_size(frames[curr]) == 2 ->
          10
      end

    updated_curr =
      cond do
        curr == 10 -> curr
        frames[curr] == nil and roll == 10 -> curr + 1
        frames[curr] == nil -> curr
        tuple_size(frames[curr]) == 1 and curr in 1..9 -> curr + 1
        true -> curr
      end

    %{frames: updated_frames, pins: updated_pins, curr: updated_curr, status: updated_status}
  end

  @spec fill_ball?(frame) :: boolean
  defp fill_ball?(tenth_frame) do
    case tenth_frame do
      {10, _} -> true
      {x, y} when x + y == 10 -> true
      _ -> false
    end
  end

  @doc """
    Returns the score of a given game of bowling if the game is complete.
    If the game isn't complete, it returns a helpful message.
  """
  @spec score(game) :: integer | String.t()
  def score(%{status: status} = _game) when status != :finished,
    do: {:error, "Score cannot be taken until the end of the game"}

  def score(game) do
    1..10
    |> Enum.map(&frame_score(game, &1))
    |> Enum.sum()
  end

  @spec frame_score(game, integer) :: integer
  defp frame_score(game, fnum) do
    case game.frames[fnum] do
      {10} -> 10 + next_two(game, fnum)
      {x, y} when x + y == 10 -> 10 + next(game, fnum)
      {x, y} -> x + y
      {x, y, z} -> x + y + z
    end
  end

  @spec next_two(game, integer) :: integer
  defp next_two(game, fnum) do
    case game.frames[fnum + 1] do
      {10} -> 10 + next(game, fnum + 1)
      {x, y} -> x + y
      {x, y, _z} -> x + y
    end
  end

  @spec next(game, integer) :: integer
  defp next(game, fnum) do
    case game.frames[fnum + 1] do
      {10} -> 10
      {x, _y} -> x
      {x, _y, _z} -> x
    end
  end
end
