defmodule BinTree do
  import Inspect.Algebra

  @moduledoc """
  A node in a binary tree.

  `value` is the value of a node.
  `left` is the left subtree (nil if no subtree).
  `right` is the right subtree (nil if no subtree).
  """
  @type t :: %BinTree{value: any, left: BinTree.t() | nil, right: BinTree.t() | nil}
  defstruct value: nil, left: nil, right: nil

  # A custom inspect instance purely for the tests, this makes error messages
  # much more readable.
  #
  # BT[value: 3, left: BT[value: 5, right: BT[value: 6]]] becomes (3:(5::(6::)):)
  def inspect(%BinTree{value: v, left: l, right: r}, opts) do
    concat([
      "(",
      to_doc(v, opts),
      ":",
      if(l, do: to_doc(l, opts), else: ""),
      ":",
      if(r, do: to_doc(r, opts), else: ""),
      ")"
    ])
  end
end

defmodule Zipper do
  # import BinTree

  @type t :: %Zipper{prev: BT.t(), current: BT.t()}
  defstruct prev: [], current: nil

  @doc """
  Get a zipper focused on the root node.
  """
  @spec from_tree(BT.t()) :: Z.t()
  def from_tree(bt) do
    %Zipper{current: bt}
  end

  @doc """
  Get the complete tree from a zipper.
  """
  @spec to_tree(Z.t()) :: BT.t()
  def to_tree(%Zipper{prev: [], current: current}), do: current
  def to_tree(z), do: z |> Zipper.up() |> to_tree

  @doc """
  Get the value of the focus node.
  """
  @spec value(Z.t()) :: any
  def value(z) do
    z.current.value
  end

  @doc """
  Get the left child of the focus node, if any.
  """
  @spec left(Z.t()) :: Z.t() | nil
  def left(%Zipper{current: %BinTree{left: nil}}), do: nil

  def left(z) do
    to_push = {:left, %{z.current | left: nil}}
    %Zipper{current: z.current.left, prev: [to_push | z.prev]}
  end

  @doc """
  Get the right child of the focus node, if any.
  """
  @spec right(Z.t()) :: Z.t() | nil
  def right(%Zipper{current: %BinTree{right: nil}}), do: nil

  def right(z) do
    to_push = {:right, %{z.current | right: nil}}
    %Zipper{current: z.current.right, prev: [to_push | z.prev]}
  end

  @doc """
  Get the parent of the focus node, if any.
  """
  @spec up(Z.t()) :: Z.t()
  def up(%Zipper{prev: []}), do: nil

  def up(%Zipper{prev: [{:left, to_pop} | t], current: current}) do
    current = %{to_pop | left: current}
    %Zipper{prev: t, current: current}
  end

  def up(%Zipper{prev: [{:right, to_pop} | t], current: current}) do
    current = %{to_pop | right: current}
    %Zipper{prev: t, current: current}
  end

  @doc """
  Set the value of the focus node.
  """
  @spec set_value(Z.t(), any) :: Z.t()
  def set_value(z, v) do
    put_in(z.current.value, v)
  end

  @doc """
  Replace the left child tree of the focus node.
  """
  @spec set_left(Z.t(), BT.t()) :: Z.t()
  def set_left(z, l) do
    put_in(z.current.left, l)
  end

  @doc """
  Replace the right child tree of the focus node.
  """
  @spec set_right(Z.t(), BT.t()) :: Z.t()
  def set_right(z, r) do
    put_in(z.current.right, r)
  end
end
