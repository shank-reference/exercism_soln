defmodule Acronym do
  @doc """
  Generate an acronym from a string.
  "This is a string" => "TIAS"
  """
  @spec abbreviate(String.t()) :: String.t()
  def abbreviate(string) do
    string
    |> String.split
    |> Enum.map(&_abbr/1)
    |> Enum.map(&String.upcase/1)
    |> Enum.join("")
  end

  defp _abbr(word) do
    clist = to_charlist(word)
    if Enum.any?(clist, &(&1 in ?A..?Z)) do
      clist
      |> Enum.filter(&(&1 in ?A..?Z))
      |> to_string
    else
      String.first(word)
    end
  end

end