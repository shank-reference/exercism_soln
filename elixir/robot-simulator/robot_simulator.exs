defmodule RobotSimulator do
  @directions [:north, :south, :east, :west]

  defguard valid_position(x, y) when is_integer(x) and is_integer(y)
  defguard valid_direction(direction) when direction in @directions

  @doc """
  Create a Robot Simulator given an initial direction and position.

  Valid directions are: `:north`, `:east`, `:south`, `:west`
  """
  @spec create(direction :: atom, position :: {integer, integer}) :: any
  def create(direction \\ nil, position \\ nil)

  def create(nil, nil), do: %{position: {0, 0}, direction: :north}

  def create(direction, {x, y} = position)
      when valid_direction(direction) and
             valid_position(x, y) do
    %{position: position, direction: direction}
  end

  def create(direction, _position) when not valid_direction(direction) do
    {:error, "invalid direction"}
  end

  def create(_direction, {x, y} = _position) when not valid_position(x, y) do
    {:error, "invalid position"}
  end

  def create(_direction, _position), do: {:error, "invalid position"}

  @doc """
  Simulate the robot's movement given a string of instructions.

  Valid instructions are: "R" (turn right), "L", (turn left), and "A" (advance)
  """
  @spec simulate(robot :: any, instructions :: String.t()) :: any
  def simulate(robot, instructions) do
    instructions
    |> to_charlist()
    |> Enum.reduce_while(robot, fn inst, robot ->
      case step(inst, robot) do
        {:error, "invalid instruction"} = err -> {:halt, err}
        updated -> {:cont, updated}
      end
    end)
  end

  defp step(inst, robot) do
    case inst do
      ?L -> turn_left(robot)
      ?R -> turn_right(robot)
      ?A -> advance(robot)
      _ -> {:error, "invalid instruction"}
    end
  end

  defp turn_left(robot) do
    new_direction =
      case robot.direction do
        :north -> :west
        :west -> :south
        :south -> :east
        :east -> :north
      end

    %{robot | direction: new_direction}
  end

  defp turn_right(robot) do
    new_direction =
      case robot.direction do
        :north -> :east
        :east -> :south
        :south -> :west
        :west -> :north
      end

    %{robot | direction: new_direction}
  end

  defp advance(robot) do
    {x, y} = robot.position

    new_position =
      case robot.direction do
        :north -> {x, y + 1}
        :east -> {x + 1, y}
        :south -> {x, y - 1}
        :west -> {x - 1, y}
      end

    %{robot | position: new_position}
  end

  @doc """
  Return the robot's direction.

  Valid directions are: `:north`, `:east`, `:south`, `:west`
  """
  @spec direction(robot :: any) :: atom
  def direction(%{direction: d}), do: d

  @doc """
  Return the robot's position.
  """
  @spec position(robot :: any) :: {integer, integer}
  def position(%{position: p}), do: p
end
