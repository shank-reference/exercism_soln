defmodule Prime do
  @doc """
  Generates the nth prime.
  """
  @spec nth(non_neg_integer) :: non_neg_integer
  def nth(count) when count < 1, do: raise(:error)

  def nth(count) do
    Stream.iterate(2, &(&1 + 1))
    |> Stream.filter(&prime?/1)
    |> Enum.take(count)
    |> List.last()
  end

  @spec prime?(n :: pos_integer) :: boolean
  defp prime?(n), do: factors(n) |> MapSet.size() == 2

  @spec factors(n :: pos_integer) :: MapSet.t(pos_integer)
  defp factors(n) do
    Enum.reduce(1..n, MapSet.new(), fn i, fact ->
      if rem(n, i) == 0, do: MapSet.put(fact, i), else: fact
    end)
  end
end
