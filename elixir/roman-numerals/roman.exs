defmodule Roman do
  @numerals %{
    1 => "I",
    4 => "IV",
    5 => "V",
    9 => "IX",
    10 => "X",
    40 => "XL",
    50 => "L",
    90 => "XC",
    100 => "C",
    400 => "CD",
    500 => "D",
    900 => "CM",
    1000 => "M"
  }

  @doc """
  Convert the number to a roman number.
  """
  @spec numerals(pos_integer) :: String.t()
  def numerals(number) do
    do_numerals(number, Map.keys(@numerals) |> Enum.reverse(), "")
  end

  defp do_numerals(0, _, roman), do: roman

  defp do_numerals(number, [head | tail], roman) when number < head do
    do_numerals(number, tail, roman)
  end

  defp do_numerals(number, [head | _tail] = numerals_list, roman) do
    do_numerals(number - head, numerals_list, roman <> @numerals[head])
  end
end
