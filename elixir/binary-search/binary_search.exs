defmodule BinarySearch do
  @doc """
    Searches for a key in the tuple using the binary search algorithm.
    It returns :not_found if the key is not in the tuple.
    Otherwise returns {:ok, index}.

    ## Examples

      iex> BinarySearch.search({}, 2)
      :not_found

      iex> BinarySearch.search({1, 3, 5}, 2)
      :not_found

      iex> BinarySearch.search({1, 3, 5}, 5)
      {:ok, 2}

  """

  @spec search(tuple, integer) :: {:ok, integer} | :not_found
  def search({}, _key), do: :not_found

  def search(numbers, key) do
    do_search(numbers, key, 0, tuple_size(numbers) - 1)
  end

  defp do_search(numbers, key, left, right) do
    m = mid(left, right)

    cond do
      elem(numbers, m) == key ->
        {:ok, m}

      right - left == 1 ->
        cond do
          elem(numbers, left) == key ->
            {:ok, left}

          elem(numbers, right) == key ->
            {:ok, right}

          true ->
            :not_found
        end

      left >= right ->
        :not_found

      key > elem(numbers, m) ->
        do_search(numbers, key, m, right)

      key < elem(numbers, m) ->
        do_search(numbers, key, left, m)
    end
  end

  defp mid(left, right), do: left + div(right - left, 2)
end
