defmodule Tournament do
  @doc """
  Given `input` lines representing two teams and whether the first of them won,
  lost, or reached a draw, separated by semicolons, calculate the statistics
  for each team's number of games played, won, drawn, lost, and total points
  for the season, and return a nicely-formatted string table.

  A win earns a team 3 points, a draw earns 1 point, and a loss earns nothing.

  Order the outcome by most total points for the season, and settle ties by
  listing the teams in alphabetical order.
  """
  @spec tally(input :: list(String.t())) :: String.t()
  def tally(input) do
    input
    |> Enum.map(&String.split(&1, ";"))
    |> Enum.filter(fn split -> length(split) == 3 end)
    |> Enum.filter(fn [_, _, res] -> res in ["win", "loss", "draw"] end)
    |> generate_stats()
    |> sort_stats()
    |> format_stats()
  end

  defp generate_stats(records) do
    stats =
      Enum.reduce(records, %{}, fn [t1, t2, res], acc ->
        t1_stat = Map.get(acc, t1, [0, 0, 0, 0, 0])
        t2_stat = Map.get(acc, t2, [0, 0, 0, 0, 0])

        t1_stat =
          case res do
            "win" -> win(t1_stat)
            "loss" -> loss(t1_stat)
            "draw" -> draw(t1_stat)
          end

        t2_stat =
          case res do
            "win" -> loss(t2_stat)
            "loss" -> win(t2_stat)
            "draw" -> draw(t2_stat)
          end

        acc |> Map.put(t1, t1_stat) |> Map.put(t2, t2_stat)
      end)

    # %{
    #   "Allegoric Alaskans" => [3, 2, 0, 1, 6],
    #   "Blithering Badgers" => [3, 1, 0, 2, 3],
    #   "Courageous Californians" => [3, 0, 1, 2, 1],
    #   "Devastating Donkeys" => [3, 2, 1, 0, ]}
    # }
    stats
    |> Map.to_list()
    |> Enum.map(fn {team, stat} -> [team | stat] end)
  end

  defp win([mp, w, d, l, p]) do
    [mp + 1, w + 1, d, l, p + 3]
  end

  defp loss([mp, w, d, l, p]) do
    [mp + 1, w, d, l + 1, p]
  end

  defp draw([mp, w, d, l, p]) do
    [mp + 1, w, d + 1, l, p + 1]
  end

  defp sort_stats(stats) do
    Enum.sort(stats, fn a, b ->
      Enum.at(a, 5) > Enum.at(b, 5) or
        (Enum.at(a, 5) == Enum.at(b, 5) and Enum.at(a, 0) < Enum.at(b, 0))
    end)
  end

  defp format_stats(stats) do
    formatted_stat_lines = stats |> Enum.map(&format_stat_line/1)

    ["Team                           | MP |  W |  D |  L |  P" | formatted_stat_lines]
    |> Enum.join("\n")
  end

  defp format_stat_line([team | stats]) do
    team_part = String.pad_trailing(team, 30)

    stats_part =
      stats
      |> Enum.map(&to_string/1)
      |> Enum.map(&String.pad_leading(&1, 2))

    [team_part | stats_part] |> Enum.join(" | ")
  end
end
