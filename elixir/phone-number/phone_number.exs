defmodule Phone do
  @doc """
  Remove formatting from a phone number.

  Returns "0000000000" if phone number is not valid
  (10 digits or "1" followed by 10 digits)

  ## Examples

  iex> Phone.number("212-555-0100")
  "2125550100"

  iex> Phone.number("+1 (212) 555-0100")
  "2125550100"

  iex> Phone.number("+1 (212) 055-0100")
  "0000000000"

  iex> Phone.number("(212) 555-0100")
  "2125550100"

  iex> Phone.number("867.5309")
  "0000000000"
  """
  @spec number(String.t()) :: String.t()
  def number(raw) do
    if Enum.all?(String.to_charlist(raw), fn c -> c not in ?a..?z end) do
      cleaned =
        raw
        |> String.replace("+1", "")
        |> String.trim_leading("1")
        |> String.replace(~r/[^\d]/, "")

      cond do
        String.length(cleaned) != 10 -> "0000000000"
        String.to_integer(String.at(cleaned, 0)) < 2 -> "0000000000"
        String.to_integer(String.at(cleaned, 3)) < 2 -> "0000000000"
        true -> cleaned
      end
    else
      "0000000000"
    end
  end

  @doc """
  Extract the area code from a phone number

  Returns the first three digits from a phone number,
  ignoring long distance indicator

  ## Examples

  iex> Phone.area_code("212-555-0100")
  "212"

  iex> Phone.area_code("+1 (212) 555-0100")
  "212"

  iex> Phone.area_code("+1 (012) 555-0100")
  "000"

  iex> Phone.area_code("867.5309")
  "000"
  """
  @spec area_code(String.t()) :: String.t()
  def area_code(raw) do
    raw
    |> number
    |> case do
      "0000000000" -> "000"
      <<area_code::binary-size(3)>> <> <<_::binary-size(3)>> <> <<_::binary-size(4)>> -> area_code
    end
  end

  @doc """
  Pretty print a phone number

  Wraps the area code in parentheses and separates
  exchange and subscriber number with a dash.

  ## Examples

  iex> Phone.pretty("212-555-0100")
  "(212) 555-0100"

  iex> Phone.pretty("212-155-0100")
  "(000) 000-0000"

  iex> Phone.pretty("+1 (303) 555-1212")
  "(303) 555-1212"

  iex> Phone.pretty("867.5309")
  "(000) 000-0000"
  """
  @spec pretty(String.t()) :: String.t()
  def pretty(raw) do
    raw
    |> number
    |> (fn num -> Regex.run(~r/(\d{3})(\d{3})(\d{4})/, num) end).()
    |> Enum.drop(1)
    |> (fn [a, b, c] -> "(#{a}) #{b}-#{c}" end).()
  end
end
