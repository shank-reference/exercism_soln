defmodule StringSeries do
  @doc """
  Given a string `s` and a positive integer `size`, return all substrings
  of that size. If `size` is greater than the length of `s`, or less than 1,
  return an empty list.
  """
  @spec slices(s :: String.t(), size :: integer) :: list(String.t())
  def slices(s, size) do
    Enum.reverse substrs(s, String.length(s), 0, size, [])
  end

  def substrs(_, _, _, len, _) when len <= 0, do: []
  def substrs(_, _, _, 0, acc), do: acc
  def substrs(_, strlen, start, len, acc) when start+len > strlen, do: acc
  def substrs(s, strlen, start, len, acc), do: substrs(s, strlen, start+1, len, [String.slice(s, start, len) | acc])
end

