defmodule Forth do
  defmodule Evaluator do
    defstruct stack: [],
              words: %{
                "+" => [:add],
                "-" => [:sub],
                "*" => [:mul],
                "/" => [:div],
                "dup" => [:dup],
                "drop" => [:drop],
                "swap" => [:swap],
                "over" => [:over]
              },

              # :normal | :def_start | :def_progress
              state: :normal,

              # nil | %{name: String.t, ops: []}
              new_word: nil
  end

  @typep word :: String.t()
  @typep stack_item :: integer | word
  @type evaluator :: %Evaluator{}

  @doc """
  Create a new evaluator.
  """
  @spec new() :: evaluator
  def new(), do: %Evaluator{}

  @doc """
  Evaluate an input string, updating the evaluator state.
  """
  @spec eval(evaluator, String.t()) :: evaluator
  def eval(ev, s) do
    # parse S
    #   downcase string
    #   split on non-word characters
    parts =
      s
      |> String.downcase()
      |> String.split(~r/[\s\p{C}]+/u)
      |> Enum.map(&parse/1)

    Enum.reduce(parts, ev, fn item, evaluator ->
      # :
      # ;
      # foo when :def_start
      # foo when :def_progress
      # swap
      # 31234
      handle_item(evaluator, item)
    end)
  end

  defp handle_item(%Evaluator{state: :normal} = evaluator, ":") do
    %{evaluator | state: :def_start}
  end

  defp handle_item(%Evaluator{state: :normal} = evaluator, item)
       when is_integer(item),
       do: %{evaluator | stack: [item | evaluator.stack]}

  defp handle_item(%Evaluator{state: :normal} = evaluator, item) do
    if Map.has_key?(evaluator.words, item) do
      updated_stack =
        Enum.reduce(evaluator.words[item], evaluator.stack, fn op, stack ->
          apply_word(stack, op)
        end)

      %{evaluator | stack: updated_stack}
    else
      raise Forth.UnknownWord
    end
  end

  defp handle_item(%Evaluator{state: :def_start}, item)
       when is_integer(item),
       do: raise(Forth.InvalidWord)

  defp handle_item(%Evaluator{state: :def_start} = evaluator, item),
    do: %{evaluator | state: :def_progress, new_word: %{name: item, ops: []}}

  defp handle_item(%Evaluator{state: :def_progress} = evaluator, ";") do
    new_word = evaluator.new_word
    words = evaluator.words

    %{
      evaluator
      | state: :normal,
        new_word: nil,
        words: Map.merge(words, %{new_word.name => new_word.ops})
    }
  end

  defp handle_item(%Evaluator{state: :def_progress} = evaluator, item)
       when is_integer(item),
       do: put_in(evaluator.new_word.ops, [item])

  defp handle_item(%Evaluator{state: :def_progress} = evaluator, item) do
    if Map.has_key?(evaluator.words, item) do
      put_in(
        evaluator.new_word.ops,
        evaluator.words[item] ++ evaluator.new_word.ops
      )
    else
      raise Forth.UnknownWord
    end
  end

  @doc """
  Return the current stack as a string with the element on top of the stack
  being the rightmost element in the string.
  """
  @spec format_stack(evaluator) :: String.t()
  def format_stack(%Evaluator{stack: []}), do: ""
  def format_stack(ev), do: ev.stack |> Enum.reverse() |> Enum.join(" ")

  @spec parse(String.t()) :: stack_item
  defp parse(item) do
    case Integer.parse(item) do
      {i, _} -> i
      :error -> item
    end
  end

  defp apply_word(stack, :add) when length(stack) < 2, do: raise(Forth.StackUnderflow)
  defp apply_word([a, b | rest], :add), do: [a + b | rest]

  defp apply_word(stack, :sub) when length(stack) < 2, do: raise(Forth.StackUnderflow)
  defp apply_word([a, b | rest], :sub), do: [b - a | rest]

  defp apply_word(stack, :mul) when length(stack) < 2, do: raise(Forth.StackUnderflow)
  defp apply_word([a, b | rest], :mul), do: [a * b | rest]

  defp apply_word(stack, :div) when length(stack) < 2, do: raise(Forth.StackUnderflow)
  defp apply_word([0 | _rest], :div), do: raise(Forth.DivisionByZero)
  defp apply_word([a, b | rest], :div), do: [div(b, a) | rest]

  defp apply_word(stack, :dup) when length(stack) < 1, do: raise(Forth.StackUnderflow)
  defp apply_word([a | rest], :dup), do: [a, a | rest]

  defp apply_word(stack, :drop) when length(stack) < 1, do: raise(Forth.StackUnderflow)
  defp apply_word([_a | rest], :drop), do: rest

  defp apply_word(stack, :swap) when length(stack) < 2, do: raise(Forth.StackUnderflow)
  defp apply_word([a, b | rest], :swap), do: [b, a | rest]

  defp apply_word(stack, :over) when length(stack) < 2, do: raise(Forth.StackUnderflow)
  defp apply_word([a, b | rest], :over), do: [b, a, b | rest]

  defp apply_word(stack, i) when is_integer(i), do: [i | stack]

  # ===== EXCEPTIONS =====

  defmodule StackUnderflow do
    defexception []
    def message(_), do: "stack underflow"
  end

  defmodule InvalidWord do
    defexception word: nil
    def message(e), do: "invalid word: #{inspect(e.word)}"
  end

  defmodule UnknownWord do
    defexception word: nil
    def message(e), do: "unknown word: #{inspect(e.word)}"
  end

  defmodule DivisionByZero do
    defexception []

    @impl true
    def exception(_value) do
      %DivisionByZero{}
    end

    @impl true
    def message(_), do: "division by zero"
  end
end
