defmodule Sieve do
  @doc """
  Generates a list of primes up to a given limit.
  """
  @spec primes_to(non_neg_integer) :: [non_neg_integer]
  def primes_to(limit) do
    2..limit |> Enum.to_list() |> sieve([])
  end

  @spec sieve([integer], [integer]) :: [integer]

  defp sieve([], primes), do: Enum.reverse(primes)

  defp sieve([prime | candidates], primes) do
    remaining = candidates |> Enum.reject(&multiple_of?(&1, prime))
    sieve(remaining, [prime | primes])
  end

  defp multiple_of?(num, prime), do: rem(num, prime) == 0
end
