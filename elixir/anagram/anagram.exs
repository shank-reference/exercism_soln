defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t(), [String.t()]) :: [String.t()]
  def match(base, candidates) do
    candidates
    |> Enum.filter(&(String.downcase(base) != String.downcase(&1)))
    |> Enum.filter(&(chars(base) == chars(&1)))
  end

  @spec chars(String.t()) :: [char]
  defp chars(word), do: word |> String.downcase() |> to_charlist |> Enum.sort()
end
