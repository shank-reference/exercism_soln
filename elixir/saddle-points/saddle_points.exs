defmodule SaddlePoints do
  @doc """
  Parses a string representation of a matrix
  to a list of rows
  """
  @spec rows(String.t()) :: [[integer]]
  def rows(str) do
    str
    |> String.split("\n")
    |> Enum.map(&String.split/1)
    |> Enum.map(&to_integers/1)
  end

  @spec to_integers([String.t()]) :: [integer]
  defp to_integers(xs), do: Enum.map(xs, &String.to_integer/1)

  @doc """
  Parses a string representation of a matrix
  to a list of columns
  """
  @spec columns(String.t()) :: [[integer]]
  def columns(str) do
    rows(str)
    |> List.zip()
    |> Enum.map(&Tuple.to_list/1)
  end

  @doc """
  Calculates all the saddle points from a string
  representation of a matrix
  """
  @spec saddle_points(String.t()) :: [{integer, integer}]
  def saddle_points(str), do: saddle_points(rows(str), columns(str))

  @spec saddle_points(rows :: [[integer]], cols :: [[integer]]) :: [{integer, integer}]
  defp saddle_points(rows, cols) do
    for {row, r} <- Enum.with_index(rows),
        {col, c} <- Enum.with_index(cols),
        Enum.max(row) == Enum.at(row, c),
        Enum.min(col) == Enum.at(col, r) do
      {r, c}
    end
  end
end
