pub fn nth(n: u32) -> u32 {
    (2..)
        .filter(|i| is_prime(*i))
        .nth(n as usize)
        .unwrap()
}

fn is_prime(n: u32) -> bool {
    let m = (n as f32).sqrt().floor() as u32 + 1;
    !(2..m).any(|i| n % i == 0)
}
