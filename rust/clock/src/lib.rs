use std::convert;
use std::fmt;

const MINUTES_IN_AN_HOUR: i32 = 60;
const MINUTES_IN_A_DAY: i32 = 24 * 60;

#[derive(Debug, PartialEq)]
pub struct Clock {
    minutes: i32,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let inital_minutes = hours * MINUTES_IN_AN_HOUR + minutes;
        let minutes = rem_wrap(inital_minutes, MINUTES_IN_A_DAY);
        Clock { minutes }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        Clock::new(0, self.minutes + minutes)
    }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let hours = self.minutes / MINUTES_IN_AN_HOUR;
        let minutes = self.minutes % MINUTES_IN_AN_HOUR;
        write!(f, "{:02}:{:02}", hours, minutes)
    }
}

fn rem_wrap(value: i32, modulus: i32) -> i32{
    ((value % modulus) + modulus) % modulus
}

impl convert::From<String> for Clock {
    fn from(s: String) -> Clock {
        let parts = s
            .split(':')
            .map(|v| v.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();

        let (hours, minutes) = match parts.as_slice() {
            [hours, minutes] => (*hours, *minutes),
            _ => panic!("unable to parse to a clock"),
        };

        Clock::new(hours, minutes)
    }
}
