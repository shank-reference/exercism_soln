use std::collections::BTreeMap;

pub fn transform(h: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    h.iter()
        .flat_map(|(points, letters)| {
            letters
                .iter()
                .map(|l| l.to_ascii_lowercase())
                .map(move |l| (l, *points))
        })
        .collect()
}
