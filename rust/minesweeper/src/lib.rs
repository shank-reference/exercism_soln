pub fn annotate(minefield: &[&str]) -> Vec<String> {
    let row_cnt: i32 = minefield.len() as i32;
    let col_cnt: i32 = if minefield.is_empty() {
        0
    } else {
        minefield[0].len() as i32
    };

    let mut result: Vec<String> = vec![];
    for (r, row_str) in minefield.iter().enumerate() {
        let mut row: Vec<String> = vec![];
        for (c, ch) in row_str.chars().enumerate() {
            if ch == '*' {
                row.push('*'.to_string());
                continue;
            }

            let mut cnt = 0;

            for rn in (r as i32 - 1)..=(r as i32 + 1) {
                for cn in (c as i32 - 1)..=(c as i32 + 1) {
                    if rn < 0 || rn > row_cnt - 1 || cn < 0 || cn > col_cnt - 1 {
                        continue;
                    }
                    if rn == r as i32 && cn == c as i32 {
                        continue;
                    }
                    if minefield[rn as usize].chars().nth(cn as usize) == Some('*') {
                        cnt += 1;
                    }
                }
            }

            if cnt == 0 {
                row.push(" ".to_string());
            } else {
                row.push(cnt.to_string());
            }
        }
        result.push(row.join(""));
    }
    result
}
