use std::iter;

pub struct PascalsTriangle {
    row_count: u32,
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        Self { row_count }
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
        match self.row_count {
            0 => vec![],
            1 => vec![vec![1]],
            n => {
                let mut result = vec![vec![1]];
                for _ in 1..n as usize {
                    let row: Vec<u32> = iter::once(1)
                        .chain(result.last().unwrap().windows(2).map(|w| w[0] + w[1]))
                        .chain(iter::once(1))
                        .collect();
                    result.push(row);
                }
                result
            }
        }
    }
}
