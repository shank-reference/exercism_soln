const EARTH_YEAR_SECONDS: f64 = 31_557_600_f64;

#[derive(Debug)]
pub struct Duration(f64);

impl From<u64> for Duration {
    fn from(s: u64) -> Self {
        Duration(s as f64)
    }
}

pub trait Planet {
    /// the length of a year on this planet (the orbial period)
    const YEAR_DURATION: f64;

    fn years_during(d: &Duration) -> f64 {
        d.0 / (Self::YEAR_DURATION * EARTH_YEAR_SECONDS)
    }
}

macro_rules! planet {
    ( $name:ident, $year_length:expr ) => {
        pub struct $name;

        impl Planet for $name {
            const YEAR_DURATION: f64 = $year_length;
        }
    };
}

planet!(Mercury, 0.2408467);
planet!(Venus, 0.61519726);
planet!(Earth, 1.0);
planet!(Mars, 1.8808158);
planet!(Jupiter, 11.862615);
planet!(Saturn, 29.447498);
planet!(Uranus, 84.016846);
planet!(Neptune, 164.79132);
