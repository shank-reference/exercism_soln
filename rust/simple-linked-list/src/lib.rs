pub struct SimpleLinkedList<T> {
    length: usize,
    head: Option<Box<Node<T>>>,
}

impl<T> SimpleLinkedList<T> {
    pub fn new() -> Self {
        Self {
            length: 0,
            head: None,
        }
    }

    pub fn len(&self) -> usize {
        self.length
    }

    pub fn push(&mut self, element: T) {
        let mut node = Node::new(element);
        node.next = self.head.take();
        self.head = Some(node);
        self.length += 1;
    }

    pub fn pop(&mut self) -> Option<T> {
        let head = self.head.take();
        if let Some(mut boxed_node) = head {
            self.head = boxed_node.next.take();
            self.length -= 1;
            Some(boxed_node.data)
        } else {
            self.head = head;
            None
        }
    }

    pub fn peek(&self) -> Option<&T> {
        match self.head {
            Some(ref node) => Some(&node.data),
            None => None,
        }
    }
}

impl<T: Clone> SimpleLinkedList<T> {
    pub fn rev(&self) -> SimpleLinkedList<T> {
        let mut result = SimpleLinkedList::new();
        let mut current_node = self.head.as_ref();
        while let Some(node_ref) = current_node {
            result.push(node_ref.data.clone());
            current_node = node_ref.next.as_ref();
        }
        result
    }
}

impl<'a, T: Clone> From<&'a [T]> for SimpleLinkedList<T> {
    fn from(item: &[T]) -> Self {
        let mut list = SimpleLinkedList::new();
        for data in item {
            list.push(data.clone());
        }
        list
    }
}

impl<T> Into<Vec<T>> for SimpleLinkedList<T> {
    fn into(mut self) -> Vec<T> {
        let mut result = vec![];
        while let Some(data) = self.pop() {
            result.push(data);
        }
        result.reverse();
        result
    }
}

struct Node<T> {
    data: T,
    next: Option<Box<Node<T>>>,
}

impl<T> Node<T> {
    fn new(data: T) -> Box<Node<T>> {
        Box::new(Node { data, next: None })
    }
}
