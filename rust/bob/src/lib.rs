enum Response {
    Question,
    Yell,
    YellQuestion,
    Silence,
    SomethingElse,
}

pub fn reply(message: &str) -> &str {
    let message = message.trim();

    let nothing_said = message.chars().all(|c| c.is_whitespace());
    let is_question = false || message.len() > 0 && message.chars().rev().next().unwrap() == '?';
    let is_alphabetic = message.chars().any(|c| c.is_alphabetic());
    let yelled = is_alphabetic && message.to_uppercase() == message;

    let resp = match (nothing_said, is_question, yelled) {
        (true, _, _) => Response::Silence,
        (false, true, true) => Response::YellQuestion,
        (false, true, false) => Response::Question,
        (false, false, true) => Response::Yell,
        _ => Response::SomethingElse,
    };

    match resp {
        Response::Question => "Sure.",
        Response::Yell => "Whoa, chill out!",
        Response::YellQuestion => "Calm down, I know what I'm doing!",
        Response::Silence => "Fine. Be that way!",
        Response::SomethingElse => "Whatever.",
    }
}
