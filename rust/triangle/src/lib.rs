use std::collections::HashSet;

pub struct Triangle([u64; 3]);

impl Triangle {
    pub fn build(mut sides: [u64; 3]) -> Option<Triangle> {
        sides.sort();

        if sides.iter().any(|&e| e == 0) || sides[2] >= sides[0] + sides[1] {
            None
        } else {
            Some(Triangle(sides))
        }
    }

    pub fn is_equilateral(&self) -> bool {
        self.n_diff_edges() == 1
    }

    pub fn is_scalene(&self) -> bool {
        self.n_diff_edges() == 3
    }

    pub fn is_isosceles(&self) -> bool {
        self.n_diff_edges() == 2
    }

    fn n_diff_edges(&self) -> usize {
        self.0.iter().collect::<HashSet<_>>().len()
    }
}
