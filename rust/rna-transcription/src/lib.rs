use std::collections::HashSet;

// DNA -> RNA
// * `G` -> `C`
// * `C` -> `G`
// * `T` -> `A`
// * `A` -> `U`

#[derive(Debug, PartialEq)]
pub struct DNA(String);

#[derive(Debug, PartialEq)]
pub struct RNA(String);

impl DNA {
    pub fn new(dna: &str) -> Result<DNA, usize> {
        validate_nucleotides(dna, "GCTA".chars().collect())?;
        Ok(DNA(dna.to_string()))
    }

    pub fn into_rna(self) -> RNA {
        let converted: String = self
            .0
            .chars()
            .map(|ch| to_rna_nucleotide(ch).unwrap())
            .collect();
        RNA::new(converted.as_ref()).unwrap()
    }
}

impl RNA {
    pub fn new(rna: &str) -> Result<RNA, usize> {
        validate_nucleotides(rna, "CGAU".chars().collect())?;
        Ok(RNA(rna.to_string()))
    }
}

fn to_rna_nucleotide(ch: char) -> Result<char, &'static str> {
    match ch {
        'G' => Ok('C'),
        'C' => Ok('G'),
        'T' => Ok('A'),
        'A' => Ok('U'),
        _ => Err("invalid nucleotide"),
    }
}

/// checks if all the nucleotides are valid.
/// In case there is an invalid nucleotide, returns the index of the same
fn validate_nucleotides(s: &str, valid_nucleotides: HashSet<char>) -> Result<(), usize> {
    if let Some(i) = s
        .chars()
        .enumerate()
        .find(|&(_i, c)| !valid_nucleotides.contains(&c))
        .map(|(i, _)| i)
    {
        Err(i)
    } else {
        Ok(())
    }
}
