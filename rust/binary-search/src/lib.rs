use std::cmp::Ordering;
use std::fmt::Debug;

pub fn find<K, S>(array: S, key: K) -> Option<usize>
where
    K: Ord + Debug,
    S: AsRef<[K]>,
{
    let array = array.as_ref();
    if array.is_empty() {
        return None;
    }
    let mid = array.len() / 2;
    match array[mid].cmp(&key) {
        Ordering::Equal => Some(mid),
        Ordering::Greater => find(&array[..mid], key),
        Ordering::Less => find(&array[mid + 1..], key).map(|p| p + mid + 1),
    }
}
