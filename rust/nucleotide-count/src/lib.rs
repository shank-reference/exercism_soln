use std::collections::HashMap;
use std::collections::HashSet;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    // valid nucleotides are [A, C, G, T]
    // check: valid nucleotide
    let valid_nucleotides: HashSet<char> = ['A', 'C', 'G', 'T'].iter().cloned().collect();
    if !valid_nucleotides.contains(&nucleotide) {
        return Err(nucleotide);
    }

    // check: valid dna
    if let Some(invalid) = dna.chars().find(|c| !valid_nucleotides.contains(c)) {
        return Err(invalid);
    }

    // do stuff and return the result
    let count = dna.chars().filter(|&c| c == nucleotide).count();
    Ok(count)
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let valid_nucleotides: HashSet<char> = ['A', 'C', 'G', 'T'].iter().cloned().collect();
    if let Some(ch) = dna.chars().find(|c| !valid_nucleotides.contains(c)) {
        return Err(ch);
    }

    let counts: HashMap<char, usize> = ['A', 'C', 'G', 'T']
        .iter()
        .map(|n| (*n, dna.chars().filter(|c| c == n).count()))
        .collect();
    Ok(counts)
}
