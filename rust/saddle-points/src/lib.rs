pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut result = Vec::new();

    let row_max: Vec<u64> = input
        .iter()
        .map(|row| row.iter().max().unwrap_or(&std::u64::MAX))
        .map(|&v| v)
        .collect();

    let n_cols = input[0].len();

    let cols: Vec<Vec<u64>> = (0..n_cols)
        .map(|c| input.iter().map(|row| row[c]).collect())
        .collect();

    let col_min: Vec<u64> = cols
        .iter()
        .map(|col| col.iter().min().unwrap_or(&std::u64::MIN))
        .map(|&v| v)
        .collect();

    for (r, row) in input.iter().enumerate() {
        for (c, item) in row.iter().enumerate() {
            if *item >= row_max[r] && *item <= col_min[c]{
                result.push((r, c));
            }
        }
    }

    result
}