pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
    // upper_bound is inclusive
    let mut is_prime: Vec<bool> = vec![true; (upper_bound + 1) as usize];

    (2..=upper_bound)
        .filter_map(|n| {
            if !is_prime[n as usize] {
                return None;
            }

            let mut num = n * 2;
            while num <= upper_bound {
                is_prime[num as usize] = false;
                num += n;
            }
            Some(n)
        })
        .collect()
}
