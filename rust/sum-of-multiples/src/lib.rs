use std::collections::HashSet;

pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    factors
        .iter()
        .filter(|f| **f != 0)
        .map(|f| multiples_under_limit(f, &limit))
        .flatten()
        .collect::<HashSet<u32>>()
        .iter()
        .sum()
}

fn multiples_under_limit(n: &u32, limit: &u32) -> Vec<u32> {
    (1..)
        .map(|i| n * i)
        .take_while(|v| v < limit)
        .collect::<Vec<_>>()
}
