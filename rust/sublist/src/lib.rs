use std::cmp::Ordering;

#[derive(Debug, PartialEq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    match first_list.len().cmp(&second_list.len()) {
        Ordering::Equal if first_list == second_list => Comparison::Equal,
        Ordering::Less if contains(second_list, first_list) => Comparison::Sublist,
        Ordering::Greater if contains(first_list, second_list) => Comparison::Superlist,
        _ => Comparison::Unequal,
    }
}

// whether `a` contains `b`
fn contains<T: PartialEq>(a: &[T], b: &[T]) -> bool {
    !a.is_empty() && b.is_empty() || a.windows(b.len()).any(|x| x == b)
}
