use std::iter::once;

pub fn build_proverb(list: &[&str]) -> String {
    if list.len() == 0 {
        return String::new();
    }

    list.iter()
        .zip(list.iter().skip(1))
        .map(|(fst, snd)| format!("For want of a {} the {} was lost.", fst, snd))
        .chain(once(format!("And all for the want of a {}.", list[0])))
        .collect::<Vec<_>>()
        .join("\n")
}
