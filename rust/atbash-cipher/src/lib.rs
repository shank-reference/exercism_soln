/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    convert(plain.to_lowercase().as_ref())
        .collect::<Vec<_>>()
        .as_slice()
        .chunks(5)
        .map(|chunk| chunk.iter().collect::<String>())
        .collect::<Vec<String>>()
        .join(" ")
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    convert(cipher).collect::<String>()
}

fn convert(text: &str) -> impl Iterator<Item = char> + '_ {
    text.chars()
        .filter(|c| c.is_ascii_alphanumeric())
        .map(|c| replace(c))
}

fn replace(c: char) -> char {
    match c {
        '0'...'9' => c,
        'a'...'z' => (b'z' - (c as u8 - b'a')) as char,
        _ => panic!("input not supported: {}", c)
    }
}
